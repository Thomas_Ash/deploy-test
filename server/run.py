import json
from datetime import datetime

from flask import Flask
from sqlalchemy import create_engine

from config import Config

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)

app = Flask(__name__)

@app.route('/api/')
def index():
    messages = engine.execute('select * from Messages;')

    return json.dumps({
        'messages': [list(x) for x in messages],
#       'messages': ['Hello World!'],
        'time': str(datetime.now())
    })

if __name__ == '__main__':
    app.run()
