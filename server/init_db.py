import time

from sqlalchemy import create_engine

from config import Config

def init_db():

    try:
        print('Connecting to "%s"...' % Config.SQLALCHEMY_DATABASE_URI)
        engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
        engine.execute('create table Messages (Text varchar(255));')
        engine.execute('insert into Messages (Text) values (\'Hello World\');')
        return True

    except (Exception,) as e:
        print(str(e))
        return False

while(not init_db()):
    print('Connection failed, waiting to retry...')
    time.sleep(5)
