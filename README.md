### I n t r o d u c t i o n

This is a simple 'Hello World' project which uses the same stack as [SYV](https://bitbucket.org/Thomas_Ash/syv/src/main/).

It was created as a reference on the deployment of such an application using docker-compose.


### S e t u p

create droplet via digitalocean web interface

open putty, create new session from ip address and ssh key


### S e c u r i t y

login as root and `apt update` & `apt upgrade -y`

`adduser syv`, enter password: create syv user

`usermod -aG sudo syv`: add user to sudo group

`cp -r ~/.ssh /home/syv/`: add ssh key

`chown -R syv:syv /home/syv/.ssh`: set owner of key

log out of putty session, log back in as syv user

`sudo ufw allow ssh`, `sudo allow http` & `sudo ufw enable`: enable firewall

`sudo nano /etc/ssh/sshd_config` and change `PermitRootLogin yes` to `PermitRootLogin no`

`sudo reboot` and log back in


### S e r v e r   -   D o c k e r

`sudo apt install docker-compose`: install docker-compose

`git clone https://Thomas_Ash@bitbucket.org/Thomas_Ash/deploy-test.git`: clone repo

`cd deploy-test` 

`sudo docker-compose build`: build containers

`sudo docker-compose up -d postgres`: start database

`sudo docker-compose run uwsgi /srv/deploy-test/server/venv/bin/python /srv/deploy-test/server/init_db.py`: create database schema

`sudo docker-compose up -d`: start remaining containers

http://<ip address> should now show Vue template.
http://<ip address>/api/ should show 'Hello World' JSON.


### S e r v e r   -   M a n u a l

`sudo apt install python3-venv build-essential python3-dev uwsgi uwsgi-plugin-python3 postgresql libpq-dev npm nginx -y`: install required packages

`sudo -u postgres psql -c "create role syv with login password '<database password>';"`: create database role

`sudo -u postgres createdb -O syv syv`: create database

`cd /usr/src`, `sudo mkdir deploy-test` & `sudo chown syv:syv deploy-test`: create directory with required ownership to clone into

`git clone https://Thomas_Ash@bitbucket.org/Thomas_Ash/deploy-test.git`: clone repo

`cd deploy-test/server`, `python3 -m venv venv` & `venv/bin/pip install wheel`: prepare python virtual environment

`venv/bin/pip install -r requirements.txt`: install required python packages

`cp config.py.example config.py`, `nano config.py` and change `~~DBPW~~` to `<database_password>`

`venv/bin/python init_db.py`: create database tables and initial rows

`cd ../client/app`, `npm install` & `npm run build`: build client app

`sudo nano /etc/nginx/nginx.conf` & change `user www-data;` to `user syv;`: set nginx to correct user

`sudo ln -s /usr/src/deploy-test/nginx/app /etc/nginx/sites-enabled`: create symlink to nginx server block

`sudo rm /etc/nginx/sites-enabled/default`: delete default nginx server block

`sudo service nginx restart`: restart nginx to apply changes

`sudo systemctl link /usr/src/deploy-test/server/app.service`: create symlink to systemd unit file

`sudo systemctl enable app` & `sudo systemctl start app`: start uwsgi

http://<ip address> should now show Vue template.
http://<ip address>/api/ should show 'Hello World' JSON.
